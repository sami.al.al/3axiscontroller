﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace _3AxisController
{
    /// <summary>
    /// Interaktionslogik für Window1.xaml
    /// </summary>
    public partial class MotorControlGUI : Window
    {
        public delegate void BtnInteractionEventHandler(int[] CorrespondingVector);
        public event BtnInteractionEventHandler GUIstartMotion;
        public event BtnInteractionEventHandler GUIstopMotion;

        int _currentLegealSpeedValue;
        int SpeedValue = 1;
        bool isValid = true;

        public MotorControlGUI()
        {
            InitializeComponent();
            TBspeed.ClearValue(TextBox.BackgroundProperty);
        }

        private void TBspeed_TextChanged(object sender, TextChangedEventArgs e)
        {
            
            isValid = Int32.TryParse(TBspeed.Text, out _currentLegealSpeedValue);
            if (!isValid)
            {
                TBspeed.Background = Brushes.Red;
                if (BtnSetSpeed != null)
                {
                    BtnSetSpeed.IsEnabled = false;
                }
            }
            else
            {
                TBspeed.Background = Brushes.Lime;
                if (BtnSetSpeed != null)
                {
                    BtnSetSpeed.IsEnabled = true;
                }
            }
        }

        private void BtnSetSpeed_Click(object sender, RoutedEventArgs e)
        {
            if (isValid)
            {
                TBspeed.ClearValue(TextBox.BackgroundProperty);
                SpeedValue = _currentLegealSpeedValue;
            }
        }

        private void ButtonDown(object sender, MouseButtonEventArgs e)
        {
            string Tag = (sender as Button).Tag.ToString();
            int[] vector;
            switch (Tag)
            {
                case "fwd":
                    vector = new int[] { SpeedValue, 0, 0 };
                    break;
                case "left":
                    vector = new int[] { 0, -SpeedValue, 0 };
                    break;
                case "right":
                    vector = new int[] { 0, SpeedValue, 0, };
                    break;
                case "bwd":
                    vector = new int[] { -SpeedValue, 0, 0 };
                    break;
                case "up":
                    vector = new int[] { 0, 0, SpeedValue };
                    break;
                case "down":
                    vector = new int[] { 0, 0, -SpeedValue };
                    break;
                default:
                    throw new Exception("this function is being called by a button that has a wrong Tag");
            }

            GUIstartMotion(vector);
        }

        private void ButtonUp(object sender, MouseButtonEventArgs e)
        {
            GUIstopMotion(new int[] { 0, 0, 0 });
        }
    }
}
