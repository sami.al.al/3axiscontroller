﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Globalization;
using System.Data;

namespace CSVto3Axis
{
    public static class CSVinput
    {
        private static string errorMsg = "error: invalid values";

        public static List<TaskStep3Axis> ReadFile(string filepath)
        {
            string[] dataInFile = File.ReadAllLines(filepath);
            List<TaskStep3Axis> AllTasks = new List<TaskStep3Axis>();

            foreach(string line in dataInFile)
            {
                string[] entries = line.Split(',');
                TaskStep3Axis ts = new TaskStep3Axis();
                try
                {
                    ts.X = Int32.Parse(entries[0]);
                    ts.Y = Int32.Parse(entries[1]);
                    ts.Z = Int32.Parse(entries[2]);
                    ts.Task = entries[3];
                }
                catch(FormatException)
                {
                    ts.Task = errorMsg;
                }

                AllTasks.Add(ts);
            }

            return AllTasks;
        }

        public static void LoadCSVdataintoDataTable(ref DataTable dt, List<TaskStep3Axis> ts3list)
        {
            for (int i = 0; i < ts3list.Count; i++)
            {
                dt.Rows.Add(ts3list[i].X, ts3list[i].Y, ts3list[i].Z, ts3list[i].Task);
            }


        }

        public static int CheckCSVdataForErrors(List<TaskStep3Axis> csvdatalist)
        // If the returned integer value equals -1 it means that no error was found. Else the returned integer value represents the index within the .csv file
        // where an invalid entry has been made.
        {
            int index = csvdatalist.FindIndex(ts => ts.Task == errorMsg);
            return index;
        }
    }

    public class TaskStepWalker
    {
        //propertiers:
        private List<TaskStep3Axis> ts3list;
        private int index = 0;

        //events:
        public delegate void NextTargetCoordinatesEventHandler(int[] coordinates);
        public event NextTargetCoordinatesEventHandler NextTargetCoordinates;

        public delegate void NextTaskEventHandler(string taskID);
        public event NextTaskEventHandler NextTask;

        public TaskStepWalker(List<TaskStep3Axis> ts3list)
        {
            this.ts3list = ts3list;
        }

        public void SetTaskStep3AxisList(List<TaskStep3Axis> ts3list)
        // Use this method to update TaskStep3AxisList
        {
            this.ts3list = ts3list;
            ResetIndex();
        }

        public int GetIndex()
        // Calling this method returns the current index, showing how far into the List Taskstepwalker has progressed
        {
            return index - 1;
        }

        public int GetProgressPercentage()
        {
            int progress = index / ts3list.Count();
            return progress;
        }

        public void ResetIndex()
        // Calling this mehtod resets TaskSteopWalker back to the  first step in the list
        {
            index = 0;
        }

        public void NextStep()
        {

            if (index == 0 || ts3list[index].X != ts3list[index - 1].X || ts3list[index].Y != ts3list[index - 1].Y || ts3list[index].Z != ts3list[index - 1].Z)
            {
                int[] coordinates = TaskStep3AxisToCoodinates(ts3list[index]);
                NextTargetCoordinates?.Invoke(coordinates);
            }

            string taskID = ts3list[index].Task;
            HandleTaskEvent(taskID, ref NextTask);

            index++;
        }

        public void LastStep()
        {
            if (index > 1 && ts3list[index-2].X != ts3list[index - 1].X || ts3list[index-2].Y != ts3list[index - 1].Y || ts3list[index-2].Z != ts3list[index - 1].Z)
            {
                int[] coordinates = TaskStep3AxisToCoodinates(ts3list[index-2]);
                NextTargetCoordinates?.Invoke(coordinates);
            }

            string taskID = ts3list[index-2].Task;
            HandleTaskEvent(taskID, ref NextTask);

            index--;
        }

        private void HandleTaskEvent(string taskID, ref NextTaskEventHandler taskEvent)
        {
            if (taskID != "n")
            {
                taskEvent?.Invoke(taskID);
            }
        }

        private int[] TaskStep3AxisToCoodinates(TaskStep3Axis ts3)
        {
            return new int[] { ts3.X, ts3.Y, ts3.Z };
        }
    }

    public static class MotorValueCoordinateConverter
    {
        public static int[] CalculateSpeedValues(int[] NextCoordinates, int[] CurrentCoordinates, int Velocity)
        // This method uses is used to determin at which velocities the motors have to move to begin and and their 3D motion the same time
        {
            int[] TravelVelocity;

            if (NextCoordinates.Length == 3 && CurrentCoordinates.Length == 3)
            {
                int[] TravelVector = NextCoordinates.Select((x, index) => x - CurrentCoordinates[index]).ToArray();
                double distance3D = Math.Sqrt(Math.Pow(TravelVector[0], 2) + Math.Pow(TravelVector[1], 2) + Math.Pow(TravelVector[2], 2));
                double factorX = Math.Abs(TravelVector[0] / distance3D);
                double factorY = Math.Abs(TravelVector[1] / distance3D);
                double factorZ = Math.Abs(TravelVector[2] / distance3D);

                TravelVelocity = new int[]{ (int)(factorX * Velocity), (int)(factorY * Velocity), (int)(factorZ * Velocity) };
            }
            else
            {
                throw new ArgumentException("One of the Coordinate arrays has a length not equal to 3.");
            }

            return TravelVelocity;
        }
    }

    public struct TaskStep3Axis
    {
        public int X;
        public int Y;
        public int Z;
        public string Task;
    }

}
