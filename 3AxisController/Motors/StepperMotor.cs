﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using CANopenMotor;
using System.Threading;
using System.Diagnostics;

namespace StepperMotorControls
{
    class Stepper : Motor
    {
        //communication stuff:
        public override SerialPort serialPort { get; set; }
        StringComparer stringComparer = StringComparer.OrdinalIgnoreCase;
        private AutoResetEvent _messageReceived;
        private bool _isConfirmed = false;
        private bool isOccupied = false;
        private string ConErrMsg = "Der E/A-Vorgang wurde wegen eines Threadendes oder einer Anwendungsanforderung abgebrochen";
        private string ConErrMsg2 = "Ein an das System angeschlossenes Gerät funktioniert nicht";
        public override bool isConfirmed
        {
            get
            {
                return _isConfirmed;
            }
        }

        //events:
        public override event ErrorOccuredEventHandler ErrorOccured;
        public override event CommunicationHappenedEventHandler CommunicationHappened;

        //Dictionairies:
        Dictionary<string, string> ErrCodetoMsg = new Dictionary<string, string>();

        //Debugging:
        Random rnd = new Random();

        public Stepper()
        {
            FillDictionaries();
        }

        private void FillDictionaries()
        {
            //Errorcodes are sourced in: https://de.nanotec.com/produkte/manual/PD6C_CANopen_USB_DE/object_dictionary%252Fod_motion_0x1003.html/#od_motion_0x1003
            // and in https://de.nanotec.com/produkte/manual/PD6C_CANopen_USB_DE/bus%252Fcan%252Fcan_stack.html/
            ErrCodetoMsg.Add("1000", "Allgemeiner Fehler");
            ErrCodetoMsg.Add("2300", "Strom am Ausgang der Steuerung zu groß");
            ErrCodetoMsg.Add("3100", "Über-/ Unterspannung am Eingang der Steuerung");
            ErrCodetoMsg.Add("4200", "Temperaturfehler innerhalb der Steuerung");
            ErrCodetoMsg.Add("6010", "Software reset (watchdog)");
            ErrCodetoMsg.Add("6100", "Interner Softwarefehler, generisch");
            ErrCodetoMsg.Add("6320", "Nennstrom muss gesetzt werden (203Bh:01h)");
            ErrCodetoMsg.Add("7121", "Motor blockiert");
            ErrCodetoMsg.Add("7305", "Inkrementaler oder Hallsensor fehlerhaft");
            ErrCodetoMsg.Add("7600", "Nichtflüchtiger Speicher voll oder korrupt, Neustart der Steuerung für Aufräumarbeiten");
            ErrCodetoMsg.Add("8000", "Fehler bei der Feldbusüberwachung");
            ErrCodetoMsg.Add("8130", "Nur CANopen: \"Life Guard\" - Fehler oder \"Heartbeat\" - Fehler");
            ErrCodetoMsg.Add("8200", "Nur CANopen: Slave brauchte zu lange um PDO Nachrichten zu Senden.");
            ErrCodetoMsg.Add("8210", "Nur CANopen: PDO wurde nicht verarbeitet aufgrund eines Längen-Fehlers");
            ErrCodetoMsg.Add("8220", "Nur CANopen: PDO Länge überschritten");
            ErrCodetoMsg.Add("8611", "Fehler in der Positionsüberwachung: Schleppfehler zu groß");
            ErrCodetoMsg.Add("8612", "Fehler in der Positionsüberwachung: Endschalter und Toleranzzone überschritten");
            ErrCodetoMsg.Add("9000", "EtherCAT: Motor fährt während Ethercat wechselt von OP -> SafeOp, PreOP usw.");
            ErrCodetoMsg.Add("0000", "success");
            ErrCodetoMsg.Add("05030000", "toggle bit not changed: Gültig nur bei \"normal transfer\" oder \"block transfer\". Das Bit, welches nach jeder Übertragung zu alternieren hat, hat seinen Zustand nicht geändert.");
            ErrCodetoMsg.Add("05040001", "command specifier unknown: Das Byte 0 des Datenblocks enthielt einen nicht zulässigen Befehl.");
            ErrCodetoMsg.Add("06010000", "unsupported access: Falls über CAN over EtherCAT (CoE) ein \"complete access\" angefordert wurde (wird nicht unterstützt.)");
            ErrCodetoMsg.Add("06010002", "read only entry: Es wurde versucht, auf ein konstantes oder nur lesbares Objekt zu schreiben.");
            ErrCodetoMsg.Add("06020000", "object not existing: Es wurde versucht, auf ein nicht vorhandenes Objekt zu zugreifen (Index fehlerhaft).");
            ErrCodetoMsg.Add("06040041", "objekt cannot be pdo mapped: Es wurde versucht, ein Objekt in das PDO zu mappen, für dass das nicht zulässig ist.");
            ErrCodetoMsg.Add("06040042", "mapped pdo exceed pdo: Würde das gewünschte Objekt in das PDO-Mapping angehängt werden, würden die 8Byte des PDO-Mappings überschritten.");
            ErrCodetoMsg.Add("06070012", "parameter length too long: Es wurde versucht, auf ein Objekt mit zu vielen Daten zu schreiben.");
            ErrCodetoMsg.Add("06070013", "parameter length too short: Es wurde versucht, auf ein Objekt mit zu wenig Daten zu schreiben.");
            ErrCodetoMsg.Add("06090011", "subindex not existing: Es wurde versucht, auf ein ungültiges Subindex eines Objektes zu zugreifen, der Index hingegen würde existieren.");
            ErrCodetoMsg.Add("06090031", "value too great: Einige Objekte unterliegen Restriktionen in der Größe des Wertes, in diesem Fall wurde versucht, einen zu hohen Wert in das Objekt zu schreiben.");
            ErrCodetoMsg.Add("06090032", "value too small: Einige Objekte unterliegen Restriktionen in der Größe des Wertes. In diesem Fall wurde versucht, einen zu niedrigen Wert in das Objekt zu schreiben.");
            ErrCodetoMsg.Add("08000000", "general error: Allgemeiner Fehler, der in keine andere Kategorie passt.");
            ErrCodetoMsg.Add("08000022", "data cannot be read or stored in this state: Die Parameter des PDOs dürfen nur im State Stopped oder \"Pre - Operational\" verändert werden.");
        }

        public override void ConnectViaCOM(string PortName)
        {
            serialPort = new SerialPort();
            _messageReceived = new AutoResetEvent(false);
            serialPort.PortName = SetPortName(serialPort.PortName, PortName);
            serialPort.BaudRate = 9600;

            TryConnectingToSerialPort(50, true);
        }

        private bool ConfirmConnection()
        {
            byte[] tst_msg = new byte[] { 78, 84, 0, 15, 5, 43, 13, 0, 0, 1, 96, 100, 0, 0, 0, 0, 0, 249, 200 };
            stableSerialPortWrite(tst_msg);
            Thread.Sleep(100);
            byte[] Answer = new byte[serialPort.BytesToRead];
            int expectedAnsLength = 23;
            if (Answer.Length == expectedAnsLength)
            {
                _isConfirmed = true;
                return true;
            }
            else
            {
                _isConfirmed = false;
                return false;
            }
        }

        public override void FaultReset()
        {
            CurrentMode = "none";
            sendMessage("6040", "00", 0x80);   
        }

        private void TryConnectingToSerialPort(int numRetries, bool confirmConnection)
        // The reason this workaround needs to be used instead of simply calling serialPort.Open() is because there is a high chance of serialPort.Open() to throw a System.IO.IOException.
        // The only and best way found around this problem is to simply retry opening the serial port until it works. So this methods will retry it as often as you tell it to through
        // (int) numRetries and when it stil doesn't connect the error will finally throw.
        {
            bool isConnected = false;
            int i = 0;
            stableSerialPortClose();

            while (!isConnected)
            {
                try
                {
                    if (serialPort.IsOpen) { serialPort.Close(); }
                    serialPort.Open();
                    isConnected = true;
                }
                catch (System.IO.IOException e)
                {
                    if (e.Message.Contains(ConErrMsg) || e.Message.Contains(ConErrMsg2))
                    {
                        Thread.Sleep(80);
                        i++;
                        Debug.WriteLine(serialPort.PortName + " retrying to connect. Try NR.: " + i.ToString());
                        if (i == numRetries) { throw; }
                    }
                    else
                    {
                        throw;
                    }
                }

            }

            if (confirmConnection)
            {
                if (ConfirmConnection())
                {
                    SetupMotor();
                    serialPort.DataReceived += SerialPort_DataReceived;
                }
                else
                {
                    serialPort.Close();
                }
            }
        }

        private void stableSerialPortClose()
        {
            bool wasCloseSuccessful = false;
            int counter = 0;
            while (wasCloseSuccessful)
            {
                try
                {
                    serialPort.Close();
                    wasCloseSuccessful = true;
                }
                catch (System.IO.IOException e)
                {
                    if(e.Message.Contains(ConErrMsg) || e.Message.Contains(ConErrMsg2))
                    {
                        if(counter > 20)
                        {
                            throw;
                        }
                    }
                    else { throw; }
                }
                counter++;
            }
        }

        private void stableSerialPortWrite(byte[] DataToWrite)
        //                                                                                      *Dokumentation: "Häufiges Problem bei der Verbindung..."
        {
            bool wasWriteSuccessful = false;
            while (!wasWriteSuccessful)
            {
                try
                {
                    serialPort.Write(DataToWrite, 0 , DataToWrite.Length);
                    wasWriteSuccessful = true;
                }
                catch (System.IO.IOException e)
                {
                    if (e.Message.Contains(ConErrMsg) || e.Message.Contains(ConErrMsg2))
                    {
                        TryConnectingToSerialPort(20, false);
                    }
                    else
                    {
                        throw;
                    }
                }
            }
        }

        private byte[] stableReadObject(short Index, byte SubIndex)
        //                                                                                      *Dokumentation: "Häufiges Problem bei der Verbindung..."
        {

            bool wasReadSuccessful = false;
            int counter = 0;
            byte[] ans = new byte[0];
            while (!wasReadSuccessful)
            {
                ans = ReadObject(Index, SubIndex);
                if(ans.Length != 0)
                {
                    wasReadSuccessful = true;
                }
                else
                {
                    TryConnectingToSerialPort(20, false);
                }
                counter++;
                if(counter == 5)
                {
                    throw new Exception("unable to read object on serialPort");
                }
            }
            return ans;
        }

        private void SetupMotor()
        // Sets up the motor to have 152400 steps in a turn. The unit for speed is set as steps per second.
        // The number 152400 is based on the sprocket and chain size, so that one step of turning produces 0.001mm of travel distance.
        // The thought behind that is further explained in the Documentation "Auslesen von *.csv-Dateien"
        {
            int defaultAcc = 5000;
            int IncPerRevolution = 152400;
            int GearRatio = 8;
            SetAcceleration(defaultAcc);
            sendMessage("608F", "01", IncPerRevolution);
            sendMessage("6091", "01", GearRatio);
            sendMessage("2061", "00", GearRatio);
            sendMessage("2062", "00", IncPerRevolution);

        }

        private void SerialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            _messageReceived.Set();
        }

        public override string sendMessage(string CommandID, string CommandSubID, int Value)
        {
            string strValue = Value.ToString("X");
            string Answer = writeMessage(CommandID + ":" + CommandSubID + "=" + strValue, true);
            return Answer;
        }

        public override string sendMessage(string CommandID, string CommandSubID)
        {
            string Answer = writeMessage(CommandID + ":" + CommandSubID, true);
            return Answer;
        }

        private void WaitForCOMportToFree()
        {
            int timeout = 3000;
            Stopwatch TimeoutTimer = new Stopwatch();
            TimeoutTimer.Start();
            while (isOccupied)
            {
                Thread.Sleep(5);
                Debug.WriteLine("com-Port is occupied");
                if (TimeoutTimer.Elapsed.TotalMilliseconds >= timeout) { throw new TimeoutException("COM port has been occupied for too long"); }
            }
            TimeoutTimer.Stop();
        }

        public string writeMessage(String message, bool waitInQueue)
        {
            if (waitInQueue) { WaitForCOMportToFree(); }
            isOccupied = true;
            string[] split = message.Split(new Char[] { ':', '=' });
            bool write = false;
            byte Bytes = 0;
            int splitcount = 0;
            if (message.Contains("="))
            {
                write = true;
            }
            short index = short.Parse(split[splitcount], System.Globalization.NumberStyles.HexNumber);
            byte SubIndex = 0;
            if (message.Contains(":"))
            {
                splitcount++;
                SubIndex = byte.Parse(split[splitcount], System.Globalization.NumberStyles.HexNumber);
            }


            var obj = stableReadObject(index, SubIndex);  //zum einfangen des Errors
            string Answer = "";

            if (obj.Length < 19)
            {
                string errCode = ByteAnswerToReadabeString(obj, true);
                string errMsg = ErrCodetoMsg[errCode.Substring(2)];
                Answer = errMsg;
                ErrorOccured(Tag, Answer);
            }
            else
            {
                string s = BitConverter.ToString(obj);
                Bytes = Convert.ToByte(obj.Length - 19);

                byte[] Value = new byte[0];
                if (write)
                {
                    Value = new byte[Bytes];
                    splitcount++;

                    if (Bytes == 1)
                    {
                        // string AnsString = HumanlyReadByteAnswer(obj);
                        Value[0] = byte.Parse(split[splitcount], System.Globalization.NumberStyles.HexNumber);
                    }

                    else if (Bytes == 2)
                    {
                        Value = BitConverter.GetBytes(Int16.Parse(split[splitcount], System.Globalization.NumberStyles.HexNumber));
                    }

                    else if (Bytes == 4)
                    {
                        Value = BitConverter.GetBytes(Int32.Parse(split[splitcount], System.Globalization.NumberStyles.HexNumber));
                    }

                }
                Answer = SDO(write, index, SubIndex, Bytes, Value);
            }
            isOccupied = false;
            return Answer;
        }

        public static string SetPortName(string defaultPortName, string InPortName)
        {
            string portName = InPortName;

            if (portName == "" || !(portName.ToLower()).StartsWith("com"))
            {
                portName = defaultPortName;
            }
            return portName;
        }

        private static byte[] CRC16(byte[] Data)
        {
            ushort Polynom = 0xA001;
            ushort Register = 0xFFFF;
            ushort temp = 0x00;

            for (int i = 0; i < Data.Length; i++)
            {
                temp = Data[i];

                for (int y = 0; y < 8; y++)
                {
                    if (((Register ^ temp) & 0x01) == 0x01)
                    {
                        Register >>= 1;
                        Register ^= Polynom;
                    }
                    else
                    {
                        Register >>= 1;
                    }
                    temp >>= 1;
                }
            }
            return new byte[2] { (byte)(Register % 256), (byte)(Register / 256) };
        }

        private byte[] ReadObject(short Index, byte SubIndex)
        {
            byte[] index = BitConverter.GetBytes(Index);
            byte[] Header = { 0x4E, 0x54, 0x00, 0x0F };
            byte[] Data = { 0x05, 0x2B, 0x0D, 0x00, 0x00, 0x01, index[1], index[0], SubIndex, 0x00, 0x00, 0x00, 0x00 };
            byte[] CRC = CRC16(Data);
            byte[] Message = new byte[Header.Length + Data.Length + CRC.Length];
            Array.Copy(Header, Message, Header.Length);
            Array.Copy(Data, 0, Message, Header.Length, Data.Length);
            Array.Copy(CRC, 0, Message, Header.Length + Data.Length, CRC.Length);
            stableSerialPortWrite(Message);
            WaitToRead(19, 7, 100);
            byte[] Answer = new byte[serialPort.BytesToRead];
            serialPort.Read(Answer, 0, serialPort.BytesToRead);
            return Answer;
        } 

        private void WriteToSerialPort(byte[] Message)
        // This method is needed as workaround for serialPort.Write which sometimes randomly throws a System.IO.IOException.
        {
            bool WriteSuccessful = false;
            int counter = 0;
            while (!WriteSuccessful)
            {
                counter++;

                try
                {
                    serialPort.Write(Message, 0, Message.Length);
                    WriteSuccessful = true;
                }
                catch (System.IO.IOException e)
                {
                    if (counter > 20)
                    {
                        throw e;
                    }
                    //Debug.WriteLine("resending Message on... " + serialPort.PortName);
                }
            }
        }

        private void WaitToRead(int MinBytesToRead, int SafetyWaitTime, int timeout)
        // before there was a Sleep(50) here. Any Answer recovered from the stepper must have a minimum of 19 Bytes. So here the system waits for the first 19 bytes to arive
        // and then gives the message an additional 7ms to receive any yet not received data. Based on some trying out and measuring receiving a singly Byte seems to take way less
        // than 1ms. almosot always all 19-30 Bytes take less than 1ms to be received as soon as they start coming in.
        {
            Stopwatch TimeoutTimer = new Stopwatch();
            TimeoutTimer.Start();
            int num = 0;
            int id = rnd.Next(100);
            while (serialPort.BytesToRead < MinBytesToRead)
            {
                if (serialPort.BytesToRead != num)
                {
                    //Debug.WriteLine(id.ToString() + "| BTR Nr.:" + serialPort.BytesToRead.ToString() + " Time:" + TimeoutTimer.ElapsedMilliseconds.ToString());
                }
                if (TimeoutTimer.Elapsed.TotalMilliseconds >= timeout) { break; }
            }
            TimeoutTimer.Stop();
            Thread.Sleep(SafetyWaitTime);

            //Debug.WriteLine(id.ToString() + "| Result Amount of BTS: " + serialPort.BytesToRead.ToString());
        }

        private string SDO(bool Write, short Index, byte SubIndex, byte Bytes, byte[] Value)
        {
            byte[] index = BitConverter.GetBytes(Index);
            Array.Reverse(index);
            byte write = 0x00;

            if (Write)
            {
                write = 0x01;
            }

            byte[] Header = { 0x4E, 0x54, 0x00, Convert.ToByte(0x0F + (Bytes * write)) };
            byte[] Command = { 0x05, 0x2B, 0x0D, write, 0x00, 0x01, index[0], index[1], SubIndex, 0x00, 0x00, 0x00, Convert.ToByte(Bytes * write) };
            byte[] Data = new byte[Command.Length + Value.Length];
            Array.Copy(Command, Data, Command.Length);
            Array.Copy(Value, 0, Data, Command.Length, Value.Length);
            byte[] CRC = CRC16(Data);
            byte[] Message = new byte[Header.Length + Data.Length + CRC.Length];
            Array.Copy(Header, Message, Header.Length);
            Array.Copy(Data, 0, Message, Header.Length, Data.Length);
            Array.Copy(CRC, 0, Message, Header.Length + Data.Length, CRC.Length);

            stableSerialPortWrite(Message);

            WaitToRead(19, 7, 100);

            byte[] Antwort = new byte[serialPort.BytesToRead];
            //Debug.WriteLine("Länge Antwort: "+Antwort.Length.ToString());
            serialPort.Read(Antwort, 0, serialPort.BytesToRead);

            string AnsString;
            if (write == 1)
            {
                if (Antwort.Length != (19))
                {
                    string errorCode = writeMessage("603F:00", false).Substring(2, 4);
                    string errMsg = ErrCodetoMsg[errorCode];
                    AnsString = "error: "+ errMsg;
                    if (errMsg != "success") { ErrorOccured?.Invoke(Tag, errMsg); }
                }
                else
                {
                    AnsString = "success";
                }
            }
            else
            {
                string s = BitConverter.ToString(Antwort);    // HIER WIEDER WEG
                s = s.Replace("-", " ");
                AnsString = ByteAnswerToReadabeString(Antwort,false);
            }

            return AnsString;
        }

        private string ReadLastError()
        {
            return "none";
        }

        private string ByteAnswerToReadabeString(byte[] ans, bool isErrorAnswer)
        {
            byte[] ModAntwort;
            int ModAntwortSize;
            int iData;
            if (isErrorAnswer) { ModAntwortSize = 4; iData = 11; }
            else{ModAntwortSize = ans.Length - 19;  iData = 17; }

            ModAntwort = new byte[ModAntwortSize];
            
            for (byte i = 0; i <= (ModAntwortSize-1); i++)
            {
                ModAntwort[i] = ans[i + iData];
            }
            Array.Reverse(ModAntwort);
            string s = BitConverter.ToString(ModAntwort);
            s = s.Replace("-", "");
            return "0x" + s;
        }

        public override void SetAcceleration(int acceleration)
        {
            sendMessage("6083", "00", acceleration);
            sendMessage("6084", "00", acceleration);
        }

        public override void SetCurrentPositionToZero()
        {
            sendMessage("6098", "00", 35);
            Reference();
            sendMessage("6098", "00", 1);
        }
    }
}
