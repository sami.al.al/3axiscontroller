﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
//using System.Numerics;
using System.Globalization;
using System.IO.Ports;
using System.Threading;
using System.Text.RegularExpressions;
using System.Diagnostics;
using CANopenMotor;
using System.Windows;

namespace LinearMotorControls
{
    class LinearMotor : Motor
    {
        //communication stuff:
        public override SerialPort serialPort { get; set; }
        private AutoResetEvent _messageReceived;
        private bool _isConfirmed = true;
        private bool isOccupied = false;
        private string Steuerzeichen = "C";
        private string lastReveivedMessage = "";

        //dictionaries:
        private Dictionary<string, string> ErrorAnsToMsg = new Dictionary<string, string>();
        private Dictionary<int, string> ErrorByteToMsg = new Dictionary<int, string>();
        private Dictionary<string, string> TransmissionErrorToMsg = new Dictionary<string, string>();
        private Dictionary<string, int> CanOpenIDtoByteSize = new Dictionary<string, int>();
        private Dictionary<int, string> ByteSizeToHexFormatting = new Dictionary<int, string>();
        private Dictionary<int, string> ByteSizeToCommandSpecifier = new Dictionary<int, string>();
        //events:
        public override event ErrorOccuredEventHandler ErrorOccured;
        public override event CommunicationHappenedEventHandler CommunicationHappened;


        public LinearMotor()
        {
            FillDictionaries();
        }

        private void FillDictionaries()
        {
            ErrorAnsToMsg.Add("0x05030000", "Toggle Bit wurde nicht geändert");
            ErrorAnsToMsg.Add("0x05040000", "SDO-Protokoll Timeout überschritten");
            ErrorAnsToMsg.Add("0x05040001", "Command Specifier ungültig oder unbekannt");
            ErrorAnsToMsg.Add("0x06010001", "Lesezugriff auf write-only Objekt");
            ErrorAnsToMsg.Add("0x06010002", "Schreibzugriff auf read-only Objekt");
            ErrorAnsToMsg.Add("0x06020000", "Objekt nicht vorhanden im Objektverzeichnis");
            ErrorAnsToMsg.Add("0x06040041", "Mapping für dieses Objekt ist nicht erlaubt");
            ErrorAnsToMsg.Add("0x06040043", "Inkompatibilität eines Parameters");
            ErrorAnsToMsg.Add("0x06060000", "Hardware Fehler");
            ErrorAnsToMsg.Add("0x06070012", "Datentyp stimmt nicht überein. Länge des Service-Parameters zu groß");
            ErrorAnsToMsg.Add("0x06090011", "Sub-Index nicht vorhanden");
            ErrorAnsToMsg.Add("0x06090030", "Wertebereich des Parameters überschritten");
            ErrorAnsToMsg.Add("0x06090031", "Wert des Parameters zu groß");
            ErrorAnsToMsg.Add("0x06090032", "Wert des Parameters ist zu klein");
            ErrorAnsToMsg.Add("0x06090042", "Anzahl und Länge der zu mappenden Parameter überschreitet PDO-Länge");
            ErrorAnsToMsg.Add("0x08000000", "Allgemeiner Fehler");
            ErrorAnsToMsg.Add("0x08000022", "Parameter kann nicht geschrieben oder gespeichert werden, aufgrund des momentanen Gerätezustandes (Betriebsart, etc.)");

            ErrorByteToMsg.Add(0, "Fehlerfrei");
            ErrorByteToMsg.Add(1, "Interner Softwarefehler");
            ErrorByteToMsg.Add(2, "Kurzschluss");
            ErrorByteToMsg.Add(3, "Leistungsendstufe-Übertemperatur");
            ErrorByteToMsg.Add(4, "Motor-Übertemperatur");
            ErrorByteToMsg.Add(5, "Interner Fehler des Motor-Kontrollers");
            ErrorByteToMsg.Add(6, "Fehler Encoder Spur A");
            ErrorByteToMsg.Add(7, "Fehler Encoder Spur B");
            ErrorByteToMsg.Add(8, "Fehler Encoder Spur Z");
            ErrorByteToMsg.Add(9, "CAN-Error");
            ErrorByteToMsg.Add(10, "Positionsabweichung zwischen Master und Slave größer als der maximal zulässige Wert");
            ErrorByteToMsg.Add(11, "Node-Guarding ausgefallen");
            ErrorByteToMsg.Add(12, "Negativ-Endschalter aktiv");
            ErrorByteToMsg.Add(13, "Positiv-Endschalter aktiv");
            ErrorByteToMsg.Add(14, "Freigabesignal fehlt");
            ErrorByteToMsg.Add(16, "Fehler in der Slave-Achse");
            ErrorByteToMsg.Add(17, "Master-Achse stromlos");
            ErrorByteToMsg.Add(18, "Watchdog Reset");
            ErrorByteToMsg.Add(19, "Zwischenkreisspannung zu niedrig");
            ErrorByteToMsg.Add(20, "Synchronisationsobjekt verspätet sich");
            ErrorByteToMsg.Add(21, "Fehler beim Löschen / Schreiben des Flash-Speichers");
            ErrorByteToMsg.Add(22, "Überlauf des Datenpuffers der Interpolation");
            ErrorByteToMsg.Add(23, "Fehler der Zeit-Synchronisation während der Interpolation");
            ErrorByteToMsg.Add(24, "Zwischenkreisspannung zu hoch");
            ErrorByteToMsg.Add(25, "Hallsensor-Fehler");
            ErrorByteToMsg.Add(26, "I²t-Strombegrenzung aktiv");
            ErrorByteToMsg.Add(27, "Nachlauffehler");
            ErrorByteToMsg.Add(28, "Encoder-Schrittverlust ");
            ErrorByteToMsg.Add(30, "Überlauf des 32-Bit-Zahlenbereichs");

            TransmissionErrorToMsg.Add("F1", "zuwenig Zeichen");
            TransmissionErrorToMsg.Add("F2", "Checksum Fehler");
            TransmissionErrorToMsg.Add("F3", "zuviele Zeichen empfangen");
            TransmissionErrorToMsg.Add("F4", "Fehler beim Umwandeln der Zeichen");

            CanOpenIDtoByteSize.Add("604000", 16);
            CanOpenIDtoByteSize.Add("604100", 16);
            CanOpenIDtoByteSize.Add("604D00", 8);
            CanOpenIDtoByteSize.Add("606000", 8);
            CanOpenIDtoByteSize.Add("606100", 8);
            CanOpenIDtoByteSize.Add("606200", 32);
            CanOpenIDtoByteSize.Add("606300", 32);
            CanOpenIDtoByteSize.Add("606400", 32);
            CanOpenIDtoByteSize.Add("606500", 32);
            CanOpenIDtoByteSize.Add("606600", 16);
            CanOpenIDtoByteSize.Add("606900", 32);
            CanOpenIDtoByteSize.Add("606A00", 16);
            CanOpenIDtoByteSize.Add("606B00", 32);
            CanOpenIDtoByteSize.Add("606C00", 32);
            CanOpenIDtoByteSize.Add("606D00", 16);
            CanOpenIDtoByteSize.Add("606E00", 16);
            CanOpenIDtoByteSize.Add("607300", 16);
            CanOpenIDtoByteSize.Add("607500", 32);
            CanOpenIDtoByteSize.Add("607A00", 32);
            CanOpenIDtoByteSize.Add("607C00", 32);
            CanOpenIDtoByteSize.Add("607D00", 8);
            CanOpenIDtoByteSize.Add("607D01", 32);
            CanOpenIDtoByteSize.Add("607D02", 32);
            CanOpenIDtoByteSize.Add("607E00", 8);
            CanOpenIDtoByteSize.Add("607F00", 32);
            CanOpenIDtoByteSize.Add("608100", 32);
            CanOpenIDtoByteSize.Add("608200", 32);
            CanOpenIDtoByteSize.Add("608300", 32);
            CanOpenIDtoByteSize.Add("608900", 8);
            CanOpenIDtoByteSize.Add("608A00", 8);
            CanOpenIDtoByteSize.Add("60FF00", 32);
            CanOpenIDtoByteSize.Add("205100", 8);
            CanOpenIDtoByteSize.Add("609800", 8);

            ByteSizeToHexFormatting.Add(8,"X2");
            ByteSizeToHexFormatting.Add(16,"X4");
            ByteSizeToHexFormatting.Add(24, "X6");
            ByteSizeToHexFormatting.Add(32,"X8");

            ByteSizeToCommandSpecifier.Add(8, "2F");
            ByteSizeToCommandSpecifier.Add(16, "2B");
            ByteSizeToCommandSpecifier.Add(24, "27");
            ByteSizeToCommandSpecifier.Add(32, "23");


        }

        public override bool isConfirmed
        {
            get
            {
                return _isConfirmed;
            }
        }

        public override void FaultReset()
        {
            sendMessage("2051", "00", 1);
            CurrentMode = "none";
        }

        public override void ConnectViaCOM(string portName)
        {
            serialPort = new SerialPort(portName, 57600, Parity.None, 8, StopBits.One);
            _messageReceived = new AutoResetEvent(false);
            serialPort.DataReceived += SerialPort_DataReceived;
            serialPort.ReadTimeout = 1000;
            serialPort.NewLine = "\r";
            serialPort.Open();
        }

        private void SerialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            string lastMsg = ReadCurrentBytesOnSerialPort();
            Debug.WriteLine("SPDR: " + lastMsg);
            switch (lastMsg[0])
            {
                case 'B':
                    lastReveivedMessage = lastMsg;
                    _messageReceived.Set();
                    break;
                case '1':
                    HandleException(lastMsg);
                    break;
                case 'F':
                    lastReveivedMessage = "Transmision fail";
                    HandleException(lastMsg);
                    _messageReceived.Set();
                    break;
            }
        }

        private void WaitForCOMportToFree()
        {
            int timeout = 2000;
            Stopwatch TimeoutTimer = new Stopwatch();
            TimeoutTimer.Start();
            while (isOccupied)
            {
                Thread.Sleep(5);
                if (TimeoutTimer.Elapsed.TotalMilliseconds >= timeout) { throw new TimeoutException("COM port has been occupied for too long"); }
            }
            TimeoutTimer.Stop();
        }

        public override string sendMessage(string CommandID, string CommandSubID, int Value)
        // overloaded verison for setting values on the motor
        {
            WaitForCOMportToFree();
            isOccupied = true;
            string data = DataChainBuilder(CommandID, CommandSubID, Value);
            string ans = sendRawMessage(data, false);
            isOccupied = false;
            return ans;
        }

        public override string sendMessage(string CommandID, string CommandSubID)
        // overloaded version for getting values on the motor
        {
            WaitForCOMportToFree();
            isOccupied = true;
            string data = DataChainBuilder(CommandID, CommandSubID);
            string ans = sendRawMessage(data, false);
            isOccupied = false;
            return ans;
        }

        public string sendRawMessage(string data, bool outputRawAnswer)
        {
            int timeout = 2000;
            byte[] ASCIIdata = DataToASCIIbyte(data);
            serialPort.Write(ASCIIdata, 0, ASCIIdata.Length);
            _messageReceived.WaitOne(timeout);
            string rawAnswer = GetLastReceivedMessage();
            CommunicationHappened?.Invoke(Tag, data, rawAnswer);
            if (!outputRawAnswer)
            {
                string Answer = InterpretNormalAnswer(rawAnswer);
                return Answer;
            }
            else
            {
                return rawAnswer;
            }
        }

        private string GetLastReceivedMessage()
        {
            string _lrm = lastReveivedMessage;
            lastReveivedMessage = "";
            return _lrm;
        }

        private string ReadCurrentBytesOnSerialPort()
        {
            string rawAnswer = serialPort.ReadLine();
            rawAnswer = rawAnswer.Trim(new char[] { '\n', '.', '\r' }); //This is done to remove the "\n.\n." That happens to randomly and unexplainedly appear on the answer sometimes.
            return rawAnswer;
        }

        //private string InterpretAnswer(string rawAnswer)
        //{
        //    char Steuerzeichen = rawAnswer[0];
        //    string Answer = "";
        //    if (Steuerzeichen == 'B' && !rawAnswer.Contains("|"))
        //    {
        //        Answer = InterpretNormalAnswer(rawAnswer);
        //    }
        //    else
        //    {
        //        Answer = InterpretComplicatedAnswer(rawAnswer);
        //    }
        //    return Answer;
        //}

        //private string InterpretComplicatedAnswer(string rawAnswer)
        //{
        //    string Answer = "";
        //    string[] ansArr = rawAnswer.Split('|');
        //    if(ansArr.Length == 1)
        //    {
        //        HandleException(rawAnswer);
        //    }
        //    else
        //    {
        //        int iAns = Array.FindIndex(ansArr, x => x[0] == 'B');
        //        int iException = Array.FindIndex(ansArr, x => x[0] != 'B');

        //        Answer = InterpretNormalAnswer(ansArr[iAns]);
        //        HandleException(ansArr[iException]);
        //    }
        //    return Answer;
        //}

        private void HandleException(string rawAnswer)
        {
            switch (rawAnswer[0])
            {
                case '1':
                    ErrorOccured(Tag, "Error has been called by: " + StringToReversedBytePackages(rawAnswer.Substring(3, 4)));
                    break;
                case 'F':
                    ErrorOccured(Tag, "Übertragungsfehler: " + TransmissionErrorToMsg[rawAnswer]);
                    break;
                default:
                    ErrorOccured(Tag, "Non-interpretable Answer received: " + rawAnswer);
                    break;
            }
        }

        private string InterpretNormalAnswer(string rawAnswer)
        // Normal Answer means any answer that comes through with Steuerzeichen being 'B'
        {
            string commandSpecifier = rawAnswer.Substring(1, 2);
            string origin = StringToReversedBytePackages(rawAnswer.Substring(3, 4));
            string data = rawAnswer.Substring(9, rawAnswer.Length - 11);
            string Answer;
            if (commandSpecifier == "80")
            {
                string errCode = StringToReversedBytePackages(data);
                string errMsg = ErrorAnsToMsg["0x" + errCode];
                Answer = "error from "+ origin +": " + errMsg;
                ErrorOccured?.Invoke(Tag, errMsg);
            }
            else if (data == "")
            {
                Answer = origin+": success";
            }
            else
            {
                Answer = "0x" + StringToReversedBytePackages(data);
            }
            return Answer;
        }

        public string StringToReversedBytePackages(string st)
        // This method takes any string, splits them in two character packeges, reverses their order an returns a new string with the reverse order.
        // For example: 
        // Input: Dönersalat ---> Splitting: Dö ne rs al at ---> Rerversing: at al rs ne Dö ---> Output: atalrsneDö
        // Any string used in this method need to cosist of an even amount of chars, else you can't split it up correctly.
        {
            if(st.Length % 2 != 0)
            {
                throw new ArgumentException("Length of String has to be even");
            }

            for (int i = 2; i < st.Length; i += 2)
            {
                st = st.Insert(i, " ");
                i++;
            }

            string[] st_array = st.Split(' ');
            Array.Reverse(st_array);
            string str_out = "";

            for (int j = 0; j < st_array.Length; j++)
            {
                str_out = str_out + st_array[j];
            }

            return str_out;
        }

        private string IntToHexString(string CommandID, string CommandSubID, int inValue)
        //This method packages any int value in the smallest possible even length hex string and outputs a string array containing it and its command specifier.
        //A more in-depth explanation for this can be found in the documentation in: "Aufbau des Übertagungsprotokolls.PNG"
        {
            string commandSpecifier;
            string outValue;
            GetOutValueAndCommandSpecifier(out commandSpecifier, out outValue, CommandID, CommandSubID, inValue);
            if (outValue.Length % 2 != 0)
            {
                outValue = "0" + outValue;
            }
            int strLength = outValue.Length;
            //if (strLength > 8)
            //{
            //    throw new ArgumentOutOfRangeException("InValue must be smaller than 4 databytes");
            //}
            string outStr = commandSpecifier + StringToReversedBytePackages(CommandID) + CommandSubID + StringToReversedBytePackages(outValue);
            return outStr;
        }

        public void GetOutValueAndCommandSpecifier(out string commandSpecifier, out string outValue, string CommandID, string CommandSubID, int inValue)
        {
            int DataByteSize;
            string CMDid = CommandID + CommandSubID;
            try
            {
                DataByteSize = CanOpenIDtoByteSize[CMDid];
                string HexFormatting = ByteSizeToHexFormatting[DataByteSize];
                outValue = inValue.ToString(HexFormatting);
                commandSpecifier = ByteSizeToCommandSpecifier[DataByteSize];
            }
            catch
            {
                commandSpecifier = "22";
                outValue = inValue.ToString("X");
                ErrorOccured(Tag, "The following command could not be found in the CanOpenIDtoByteSize Dictionary. Please add it to ensure stability: " + CommandID + ":" + CommandSubID);
            }
        }

        public string DataChainBuilder(string CommandID, string CommandSubID, int Value)
        // This method and the overloaded version of it convert simple commands for the motor the the more complex data chain, that gets send via COM-port.
        // The way this datachain is put together is expained in the documentation in: "Aufbau des Übertagungsprotokolls.PNG"

        {
            string Data = IntToHexString(CommandID, CommandSubID, Value);
            string ChkSum = CalculateCheckSum(Data);
            return Steuerzeichen + Data + ChkSum;
        }

        public string DataChainBuilder(string CommandID, string CommandSubID)
        // for reading motor data rather than writing data this method needs to be used.
        {
            string Data = "40" + StringToReversedBytePackages(CommandID) + CommandSubID;
            string ChkSum = CalculateCheckSum(Data);
            return Steuerzeichen + Data + ChkSum;
        }

        public string CalculateCheckSum(string hexValue)
        // This method calucates the CheckSum8 2s Complement of any given hexadecimal value
        {
            byte[] buf = SoapHexBinary.Parse(hexValue).Value;

            int chkSum = buf.Aggregate(0, (s, b) => s += b) & 0xff;
            chkSum = (0x100 - chkSum) & 0xff;

            var str = chkSum.ToString("X2");
            return str;
        }

        public byte[] DataToASCIIbyte(string data)
        {
            byte[] first = Encoding.ASCII.GetBytes(data);
            byte[] second = new byte[] { 0x0d, 0x0a, 0x2e };
            byte[] ret = new byte[first.Length + second.Length];
            Buffer.BlockCopy(first, 0, ret, 0, first.Length);
            Buffer.BlockCopy(second, 0, ret, first.Length, second.Length);
            return ret;
        }

        public int InitiateFreeFromEndswitch()
        {
            CurrentMode = "FreeFromEndswitch";
            string lmop = sendMessage("6061", "00");
            int lastModeOfOP = Convert.ToInt32(lmop, 16);

            string ans = sendMessage("6060", "00", 0xFD);
            return lastModeOfOP;
        }

        public override void SetAcceleration(int acceleration)
        {
            sendMessage("6083", "00", acceleration);
        }

        public string ReadCurrentError()
        {
            string ans = sendMessage("2052", "00");
            int iErr = Convert.ToInt32(ans, 16);
            string ErrorMessage = ErrorByteToMsg[iErr];
            return ErrorMessage;
        }

        public void FreeFromEndswitch(int lastModeOfOP)
        {
            string ans = sendMessage("2051", "00", 1);
            ans = sendMessage("6040", "00", 0x00);
            ans = sendMessage("6040", "00", 0xD0);
            ans = sendMessage("6040", "00", 0x06);
            ans = sendMessage("6040", "00", 0x07);
            ans = sendMessage("6040", "00", 0x0F);
            ans = sendMessage("6040", "00", 0xDF);
            //Thread.Sleep(1000);
            //sendMessage("6060", "00", lastModeOfOP);
        }

        public override void SetCurrentPositionToZero()
        {
            sendMessage("6098", "00", 34);
            Reference();
            sendMessage("6098", "00", 2);
        }

    }
}
