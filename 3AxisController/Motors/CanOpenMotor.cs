﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;

namespace CANopenMotor
{
    abstract class Motor
    {
        public abstract bool isConfirmed { get; }
        public abstract SerialPort serialPort { get; set; }
        public abstract string sendMessage(string CommandID, string CommandSubID, int Value);
        public abstract string sendMessage(string CommandID, string CommandSubID);
        public abstract void ConnectViaCOM(string COM);
        public abstract void SetAcceleration(int acceleration);
        public abstract void FaultReset();
        public abstract void SetCurrentPositionToZero();

        public delegate void ErrorOccuredEventHandler(string Tag,string ErrorMEssage);
        public abstract event ErrorOccuredEventHandler ErrorOccured;

        public delegate void CommunicationHappenedEventHandler(string Tag, string ComOut, string ComIn);
        public abstract event CommunicationHappenedEventHandler CommunicationHappened;

        public string CurrentMode = "none";
        public string Tag = "none";

        public void ClosePort()
        {
            serialPort.Close();
        }

        public bool IsOpen
        {
            get
            {
                try
                {
                    return serialPort.IsOpen;
                }
                catch (NullReferenceException)
                {
                    return false;
                }
            }
        }

        public void MoveAbs(int Position)
        {
            CurrentMode = "MoveAbs";
            sendMessage("6060", "00", 0x01);
            sendMessage("6040", "00", 0x06);
            sendMessage("6040", "00", 0x07);
            sendMessage("6040", "00", 0x0F);
            int currentPos = GetPosition();
            int distance = Position - currentPos;
            sendMessage("607A", "00", distance);
            sendMessage("6040", "00", 0x5F);
        }

        public void MoveRel(int Position)
        {
            CurrentMode = "MoveRel";
            sendMessage("6060", "00", 0x01);
            sendMessage("6040", "00", 0x06);
            sendMessage("6040", "00", 0x07);
            sendMessage("6040", "00", 0x0F);
            sendMessage("607A", "00", Position);
            sendMessage("6040", "00", 0x5F);
        }

        public void SetMoveVelocity(int velocity)
        {
            sendMessage("6081", "00", velocity);
        }

        public void StopMotion()
        {
            CurrentMode = "Stop";
            sendMessage("6040", "00", 0x07);
        }

        public void Reference()
        {
            CurrentMode = "Reference";
            sendMessage("6060", "00", 0x06);
            sendMessage("6040", "00", 0x06);
            sendMessage("6040", "00", 0x07);
            sendMessage("6040", "00", 0x0F);
            sendMessage("6040", "00", 0x5F);
        }

        public void VelocityMode(int velocity)
        {
            CurrentMode = "VelocityMode";

            sendMessage("6060", "00", 0x0003);
            sendMessage("6040", "00", 0x0006);
            sendMessage("6040", "00", 0x0007);
            sendMessage("6040", "00", 0x000F);
            sendMessage("60FF", "00", velocity);
            sendMessage("6040", "00", 0x005F);
        }

        public void SetTargetVelocity(int velocity)
        {
            sendMessage("60FF", "00", velocity);
        }

        public int GetPosition()
        {
            string pos = sendMessage("6064", "00");
            int currentPos = Convert.ToInt32(pos, 16);
            return currentPos;
        }

        public void GetStatus()
        {
            throw new NotImplementedException();
        }

        public int GetTargetVelocity()
        {
            int TargetVelocity = Convert.ToInt32(sendMessage("60FF", "00"), 16);
            return TargetVelocity;
        }

        public int GetMoveVelocity()
        {
            int MoveVelocity = Convert.ToInt32(sendMessage("6081", "00"), 16);
            return MoveVelocity;
        }
    }
}
