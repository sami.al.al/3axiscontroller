﻿using SlimDX.XInput;
using System;
using System.Management;

namespace slimdx_gamepad
{

    public struct DPad
    {
        public bool Up;
        public bool Down;
        public bool Left;
        public bool Right;
    };

    public struct Thumbstick
    {
        public bool Click;
        public double X;
        public double Y;
    };

    public struct Thumbsticks
    {
        public Thumbstick Left;
        public Thumbstick Right;
    };

    class Gamepad
    {

        private bool _isConnected = false;
        public bool isConnected
        {
            get { return _isConnected; }
        }
        public bool updateLeftThumbstick = true;
        public bool updateRightThumbstick = true;
        public bool updateLeftTrigger = true;
        public bool updateRightTrigger = true;

        private double _deadzone;
        public double Deadzone
        {
            get { return _deadzone; }
            set { _deadzone = value; }
        }
        
        public delegate void BtnStateChanged();
        //Buttonspress events
        public event BtnStateChanged BtnAPressed;
        public event BtnStateChanged BtnBPressed;
        public event BtnStateChanged BtnXPressed;
        public event BtnStateChanged BtnYPressed;
        public event BtnStateChanged BtnStartPressed;
        public event BtnStateChanged BtnBackPressed;
        public event BtnStateChanged BtnLBPressed; //LB = LeftBumper
        public event BtnStateChanged BtnRBPressed; //RB = Right Bumper
        public event BtnStateChanged BtnLeftThumbPressed;
        public event BtnStateChanged BtnRightThumbPressed;


        //Buttonrelease events
        public event BtnStateChanged BtnAReleased;
        public event BtnStateChanged BtnBReleased;
        public event BtnStateChanged BtnXReleased;
        public event BtnStateChanged BtnYReleased;
        public event BtnStateChanged BtnStartReleased;
        public event BtnStateChanged BtnBackReleased;
        public event BtnStateChanged BtnLBReleased;
        public event BtnStateChanged BtnRBReleased;
        public event BtnStateChanged BtnLeftThumbReleased;
        public event BtnStateChanged BtnRightThumbReleased;

        //Triggervaluechange events:
        public delegate void TriggerValueChangeEventHandler(double NewTriggerValue);
        public event TriggerValueChangeEventHandler LeftTriggerValueChange;
        public event TriggerValueChangeEventHandler RightTriggerValueChange;

        //Thumbstickvaluechange events:
        public delegate void ThumbstickValueChangeEventHandler(Thumbstick NewThumbstickState);
        public event ThumbstickValueChangeEventHandler LeftThumbstickValueChange;
        public event ThumbstickValueChangeEventHandler RightThumbstickValueChange;

        //DpadValueChange events:
        public delegate void DPadValueChangedEventHandler(DPad NewDPadState);
        public event DPadValueChangedEventHandler DPadValueChange;

        //connection status events:
        public delegate void ConnectionStatusChangedEventHandler(bool isConnected);
        public event ConnectionStatusChangedEventHandler ConnectionStatusChanged;


        private Controller _controller;

        private void SetupEventAndSet(bool value, ref bool internalButtonValue, ref BtnStateChanged BtnPressedEvent, ref BtnStateChanged BtnReleasedEvent)
        {
            if (value != internalButtonValue)
            {
                switch (value)
                {
                    case true:
                        BtnPressedEvent?.Invoke();
                        break;
                    case false:
                        BtnReleasedEvent?.Invoke();
                        break;
                }
            }
            internalButtonValue = value;
        }

        //internal Button Values:
        private bool _a;
        private bool _b;
        private bool _x;
        private bool _y;
        private bool _start;
        private bool _back;
        private bool _leftBumper;
        private bool _rightBumper;

        public bool A
        {
            get
            {
                return _a;
            }

            set
            {
                SetupEventAndSet(value, ref _a, ref BtnAPressed, ref BtnAReleased);
            }
        }

        public bool B
        {
            get
            {
                return _b;
            }

            set
            {
                SetupEventAndSet(value, ref _b, ref BtnBPressed, ref BtnBReleased);
            }
        }

        public bool X
        {
            get
            {
                return _x;
            }

            set
            {
                SetupEventAndSet(value, ref _x, ref BtnXPressed, ref BtnXReleased);
            }
        }

        public bool Y
        {
            get
            {
                return _y;
            }

            set
            {
                SetupEventAndSet(value, ref _y, ref BtnYPressed, ref BtnYReleased);
            }
        }

        public bool Start
        {
            get
            {
                return _start;
            }

            set
            {
                SetupEventAndSet(value, ref _start, ref BtnStartPressed, ref BtnStartReleased);
            }
        }

        public bool Back
        {
            get
            {
                return _back;
            }

            set
            {
                SetupEventAndSet(value, ref _back, ref BtnBackPressed, ref BtnBackReleased);
            }
        }
        
        public bool LeftBumper
        {
            get
            {
                return _leftBumper;
            }

            set
            {
                SetupEventAndSet(value, ref _leftBumper, ref BtnLBPressed, ref BtnLBReleased);
            }
        }

        public bool RightBumper
        {
            get
            {
                return _rightBumper;
            }

            set
            {
                SetupEventAndSet(value, ref _rightBumper, ref BtnRBPressed, ref BtnRBReleased);
            }
        }

        public double LeftTrigger;
        public double RightTrigger;
        public Thumbsticks Thumbsticks;

        public DPad DPad;


        public Gamepad(Controller controller)
        {
            _controller = controller;
            Deadzone = 0.1;
        }

        public Gamepad(double Deadzone)
        {
            _controller = new Controller(UserIndex.One);
            _isConnected = _controller.IsConnected;

            this.Deadzone = Deadzone;

            var watcher = new ManagementEventWatcher();
            var query = new WqlEventQuery("SELECT * FROM Win32_DeviceChangeEvent WHERE EventType = 2  OR EventType = 3");
            watcher.EventArrived += USBdeviceConnectionStatusChanged;
            watcher.Query = query;
            watcher.Start();
        }

        public string GetBatteryStatus()
        {
            var BatteryInfo = _controller.GetBatteryInformation(new BatteryDeviceType());
            string LvlStr = BatteryInfo.Level.ToString();
            return LvlStr;
        }

        private void USBdeviceConnectionStatusChanged(object sender, EventArrivedEventArgs e)
        {
            bool beforeStatus = _isConnected;
            getStateAndUpdateConnectionStatus();
            if(_isConnected != beforeStatus)
            {
                ConnectionStatusChanged?.Invoke(_isConnected);
            }
        }

        private SlimDX.XInput.Gamepad getStateAndUpdateConnectionStatus()
        {
            SlimDX.XInput.Gamepad gpad =  new SlimDX.XInput.Gamepad();
            try
            {
                
                gpad = _controller.GetState().Gamepad;
                _isConnected = true;
            }
            catch (XInputException)
            {
                _isConnected = false;
            }

            return gpad;
        }

        /// <summary>
        /// Load the current state
        /// Places values in more JS friendly format
        /// </summary>
        public void LoadState()
        {
            SlimDX.XInput.Gamepad state = getStateAndUpdateConnectionStatus();

            //Upadate Buttons:
            A = ((state.Buttons & GamepadButtonFlags.A) == GamepadButtonFlags.A);
            B = ((state.Buttons & GamepadButtonFlags.B) == GamepadButtonFlags.B);
            X = ((state.Buttons & GamepadButtonFlags.X) == GamepadButtonFlags.X);
            Y = ((state.Buttons & GamepadButtonFlags.Y) == GamepadButtonFlags.Y);
            Start = ((state.Buttons & GamepadButtonFlags.Start) == GamepadButtonFlags.Start);
            Back = ((state.Buttons & GamepadButtonFlags.Back) == GamepadButtonFlags.Back);
            LeftBumper = ((state.Buttons & GamepadButtonFlags.LeftShoulder) == GamepadButtonFlags.LeftShoulder);
            RightBumper = ((state.Buttons & GamepadButtonFlags.RightShoulder) == GamepadButtonFlags.RightShoulder);

            //Update DPad:
            bool NewDPadUP = ((state.Buttons & GamepadButtonFlags.DPadUp) == GamepadButtonFlags.DPadUp);
            bool NewDPAdDown = ((state.Buttons & GamepadButtonFlags.DPadDown) == GamepadButtonFlags.DPadDown);
            bool NewDPadLeft = ((state.Buttons & GamepadButtonFlags.DPadLeft) == GamepadButtonFlags.DPadLeft);
            bool NewDPadRight = ((state.Buttons & GamepadButtonFlags.DPadRight) == GamepadButtonFlags.DPadRight);
            if((DPad.Up != NewDPadUP) || (DPad.Down != NewDPAdDown) || (DPad.Left != NewDPadLeft) || (DPad.Right != NewDPadRight))
            {
                DPad.Up = NewDPadUP;
                DPad.Down = NewDPAdDown;
                DPad.Left = NewDPadLeft;
                DPad.Right = NewDPadRight;
                DPadValueChange?.Invoke(DPad);
            }
            DPad.Up = NewDPadUP;
            DPad.Down = NewDPAdDown;
            DPad.Left = NewDPadLeft;
            DPad.Right = NewDPadRight;

            //Update Triggers:
            if (updateLeftThumbstick) { updateTrigger(state.LeftTrigger, ref LeftTrigger, ref LeftTriggerValueChange); }
            if (updateRightTrigger) { updateTrigger(state.RightTrigger, ref RightTrigger, ref RightTriggerValueChange); }
            

            //Update Thumbsticks:
            updateThumbsticksAndHandleEvents(state);

        }


        private void updateTrigger(byte NewTrigger, ref double Trigger , ref TriggerValueChangeEventHandler TriggerEvent)
        {
            double NewTriggerValue;
            if (NewTrigger > SlimDX.XInput.Gamepad.GamepadTriggerThreshold)
            {
                NewTriggerValue = NewTrigger / 255.0;
            }
            else
            {
                NewTriggerValue = 0;
            }

            if(NewTriggerValue != Trigger)
            {
                Trigger = NewTriggerValue;
                TriggerEvent?.Invoke(NewTriggerValue);
            }
            Trigger = NewTriggerValue;
        }


        private void updateThumbsticksAndHandleEvents(SlimDX.XInput.Gamepad state)
        {
            if (updateLeftThumbstick)
            {
                bool NewLeftThumbIsClicked = ((state.Buttons & GamepadButtonFlags.LeftThumb) == GamepadButtonFlags.LeftThumb);
                SetupEventAndSet(NewLeftThumbIsClicked, ref Thumbsticks.Left.Click, ref BtnLeftThumbPressed, ref BtnLeftThumbReleased);
                Vector2 leftStick = new Vector2(state.LeftThumbX, state.LeftThumbY).Normalize(SlimDX.XInput.Gamepad.GamepadLeftThumbDeadZone);
                HandleThumbStickEventAndApplyDeadzone(ref leftStick.X, ref leftStick.Y, ref Thumbsticks.Left, ref LeftThumbstickValueChange);
            }
            if (updateRightThumbstick)
            {
                bool NewRightThumbIsClicked = ((state.Buttons & GamepadButtonFlags.RightThumb) == GamepadButtonFlags.RightThumb);
                SetupEventAndSet(NewRightThumbIsClicked, ref Thumbsticks.Right.Click, ref BtnRightThumbPressed, ref BtnRightThumbReleased);
                Vector2 rightStick = new Vector2(state.RightThumbX, state.RightThumbY).Normalize(SlimDX.XInput.Gamepad.GamepadRightThumbDeadZone);
                HandleThumbStickEventAndApplyDeadzone(ref rightStick.X, ref rightStick.Y, ref Thumbsticks.Right, ref RightThumbstickValueChange);
            }
        }

        private void HandleThumbStickEventAndApplyDeadzone(ref double NewX, ref double NewY, ref Thumbstick _tbs, ref ThumbstickValueChangeEventHandler Thumbevent)
        {
            ApplyDeadzone(ref NewX, ref NewY);
            if((_tbs.X != NewX)||(_tbs.Y != NewY))
            {
                _tbs.X = NewX;
                _tbs.Y = NewY;
                Thumbevent?.Invoke(_tbs);
            }
            _tbs.X = NewX;
            _tbs.Y = NewY;
        }

        private void ApplyDeadzone(ref double X, ref double Y)
        {
            double hypothnuse = Math.Sqrt(Math.Pow(X, 2) + Math.Pow(Y, 2));
            if(hypothnuse < Deadzone)
            {
                X = 0;
                Y = 0;
            }
        }
    }
}
