﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Command2TextBox;
using CSVto3Axis;
using StepperMotorControls;
using LinearMotorControls;
using slimdx_gamepad;
using CANopenMotor;
using System.Diagnostics;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Data;
using Microsoft.Win32;

namespace _3AxisController
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        //interaction objects:
        Gamepad _gamepad;
        MotorControlGUI _motorControlGUI;
        Stepper MotorX1 = new Stepper();
        Stepper MotorX2 = new Stepper();
        Stepper MotorY = new Stepper();
        LinearMotor MotorZ = new LinearMotor();
        List<Motor> AllMotors = new List<Motor>();

        //timers:
        System.Timers.Timer ControllerUpdateTimer = new System.Timers.Timer();
        System.Timers.Timer PositionUpdateTimer = new System.Timers.Timer();

        //messages:
        string ConnectionErrorHeader = "Verbindung fehlgeschlagen";
        string MotorNotReady = "Motor ist nicht bereit für die Bewegung";
        string WrongMotor = "COM Port existiert, ist aber nicht das richtige Gerät";
        string ConnectedMessage = "verbunden";
        string NotConnectedMessage = "nicht verbunden";
        string FreeMotorMessage = "Power Knopf auf dem MC-1 40 betätigen und OK klicken!";
        string InstructonHeader = "Anweisung";

        //counters:
        int startupcounter = 0;

        //Dictionaries:
        Dictionary<string, ComboBox> TagToComboBox = new Dictionary<string, ComboBox>();
        Dictionary<string, Button> TagToButton = new Dictionary<string, Button>();
        Dictionary<string, Label> TagToLabel = new Dictionary<string, Label>();
        Dictionary<string, Motor> TagToMotor = new Dictionary<string, Motor>();
        Dictionary<Motor, Label> MotorToPositionLabel = new Dictionary<Motor, Label>();
        Dictionary<int, Motor> IndexToMotor = new Dictionary<int, Motor>();

        //Datatables:
        DataTable TaskStepDataTable = new DataTable();

        //Calibrations:
        const int factor = 1000; // micrometer to millimeter

        int XaxisGampadMaxSpeed = 500000;
        int YaxisGampadMaxSpeed = 500000;
        int ZaxisGamepadMaxSpeed = 100000;
        int GampadTimerUpdateInterval = 100;
        int PositionUpdateTimerInterval = 500;

        int _XaxisMotorAcceleration = 10000;
        public int XaxisMotorAcceleration
        {
            get { return _XaxisMotorAcceleration / factor; }
            set
            {
                UpdateBoundMotorVariable(value, ref _XaxisMotorAcceleration, "XaxisMotorAcceleration");
                if (MotorX1.IsOpen) { MotorX1.SetAcceleration(_XaxisMotorAcceleration); }
                if (MotorX2.IsOpen) { MotorX2.SetAcceleration(_XaxisMotorAcceleration); }
            }
        }

        int _YaxisMotorAcceleration = 10000;
        public int YaxisMotorAcceleration
        {
            get { return _YaxisMotorAcceleration / factor; }
            set
            {
                UpdateBoundMotorVariable(value, ref _YaxisMotorAcceleration, "YaxisMotorAcceleration");
                if (MotorY.IsOpen) { MotorY.SetAcceleration(_YaxisMotorAcceleration); }
            }
        }

        int _ZaxisMotorAcceleration = 1000000;
        public int ZaxisMotorAcceleration
        {
            get { return _ZaxisMotorAcceleration / factor; }
            set
            {
                UpdateBoundMotorVariable(value, ref _ZaxisMotorAcceleration, "ZaxisMotorAcceleration");
                if (MotorZ.IsOpen) { MotorZ.SetAcceleration(_ZaxisMotorAcceleration); }
            }
        }

        int _ZmeasureSpeed = 10000;
        public int ZmeasureSpeed
        {
            get { return _ZmeasureSpeed / factor; }
            set { UpdateBoundMotorVariable(value, ref _ZmeasureSpeed, "ZmeasureSpeed");}
        }

        int _MoveSpeed = 100000;
        public int MoveSpeed
        {
            get { return _MoveSpeed / factor; }
            set { UpdateBoundMotorVariable(value, ref _MoveSpeed, "MoveSpeed"); }
        }

        int[] CurrentTypedPosition = new int[3];
        public int CurrentTypedXPosition
        {
            get { return CurrentTypedPosition[0] / factor; }
            set { UpdateBoundMotorVariable(value, ref CurrentTypedPosition[0], "CurrentTypedXPosition"); }
        }
        public int CurrentTypedYPosition
        {
            get { return CurrentTypedPosition[1] / factor; }
            set { UpdateBoundMotorVariable(value, ref CurrentTypedPosition[1], "CurrentTypedYPosition"); }
        }
        public int CurrentTypedZPosition
        {
            get { return CurrentTypedPosition[2] / factor; }
            set { UpdateBoundMotorVariable(value, ref CurrentTypedPosition[2], "CurrentTypedZPosition"); }
        }


        private void UpdateBoundMotorVariable(int value, ref int privateVar, string PropertyName)
        {
            //if(value != privateVar)
            //{
                privateVar = value*factor;
                OnPropertyChanged(PropertyName);
            //}
        }

        public MainWindow()
        {
            DataContext = this;
            InitializeComponent();
            Console.SetOut(new MultiTextWriter(new ControlWriter(ConsoleText), Console.Out));
            SetupController();
            FillDictionariesAndLists();
            SetupTimers();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged([CallerMemberName] string PropertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(PropertyName));
        }

        private void SetupTimers()
        {
            SetupControllerUpdateTimer();
            SetupPositionUpdateTimer();
            PositionUpdateTimer.Start();
        }

        private void SetupPositionUpdateTimer()
        {
            PositionUpdateTimer.Interval = PositionUpdateTimerInterval;
            PositionUpdateTimer.Elapsed += PositionUpdateTimer_Elapsed;
            PositionUpdateTimer.AutoReset = true;
            
        }

        private void PositionUpdateTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            foreach (Motor m in AllMotors)
            {
                if (m.IsOpen && MotorToPositionLabel.ContainsKey(m))
                {
                    int pos = m.GetPosition() / 1000;
                    MotorToPositionLabel[m].Dispatcher.Invoke(new Action(() =>
                    {
                        MotorToPositionLabel[m].Content = pos.ToString();
                    }));
                }
            }
        }

        private void SetupControllerUpdateTimer()
        {
            ControllerUpdateTimer.Interval = GampadTimerUpdateInterval;
            ControllerUpdateTimer.Elapsed += ControllerUpdateTimer_Elapsed;
            ControllerUpdateTimer.AutoReset = true;
        }

        private void ControllerUpdateTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            _gamepad.LoadState();
        }

        private void FillDictionariesAndLists()
        {
            TagToComboBox.Add("X1", CBconnectionX1);
            TagToComboBox.Add("X2", CBconnectionX2);
            TagToComboBox.Add("Y", CBconnectionY);
            TagToComboBox.Add("Z", CBconnectionZ);

            TagToButton.Add("X1", BtnRetryConnectX1);
            TagToButton.Add("X2", BtnRetryConnectX2);
            TagToButton.Add("Y", BtnRetryConnectY);
            TagToButton.Add("Z", BtnRetryConnectZ);

            TagToMotor.Add("X1", MotorX1);
            TagToMotor.Add("X2", MotorX2);
            TagToMotor.Add("Y", MotorY);
            TagToMotor.Add("Z", MotorZ);

            TagToLabel.Add("X1", LblconnectionX1);
            TagToLabel.Add("X2", LblconnectionX2);
            TagToLabel.Add("Y", LblconnectionY);
            TagToLabel.Add("Z", LblconnectionZ);

            MotorToPositionLabel.Add(MotorX1, LblCoordinateX);
            MotorToPositionLabel.Add(MotorY, LblCoordinateY);
            MotorToPositionLabel.Add(MotorZ, LblCoordinateZ);

            AllMotors.Add(MotorX1);
            AllMotors.Add(MotorX2);
            AllMotors.Add(MotorY);
            AllMotors.Add(MotorZ);

            IndexToMotor.Add(0, MotorX1);
            IndexToMotor.Add(1, MotorY);
            IndexToMotor.Add(2, MotorZ);
        }

        private void SetupController()
        {
            _gamepad = new Gamepad(0.05);
            _gamepad.ConnectionStatusChanged += _gamepad_ConnectionStatusChanged;
            _gamepad.LeftThumbstickValueChange += _gamepad_LeftThumbstickValueChange;
            _gamepad.RightThumbstickValueChange += _gamepad_RightThumbstickValueChange;
            _gamepad.LeftTriggerValueChange += _gamepad_LeftTriggerValueChange;
            _gamepad.RightTriggerValueChange += _gamepad_RightTriggerValueChange;
            _gamepad.BtnAPressed += _gamepad_BtnA;
            _gamepad.BtnAReleased += _gamepad_BtnA;
            _gamepad_ConnectionStatusChanged(_gamepad.isConnected);
            UpdateGUIControllerBatteryStatus();
        }

        private void _gamepad_BtnA()
        {
            _gamepad_LeftThumbstickValueChange(_gamepad.Thumbsticks.Left);
            //_gamepad_RightThumbstickValueChange(_gamepad.Thumbsticks.Right);
            //_gamepad_LeftTriggerValueChange(_gamepad.LeftTrigger);
            //_gamepad_RightTriggerValueChange(_gamepad.RightTrigger);
        }

        private void _gamepad_RightTriggerValueChange(double NewTriggerValue)
        {
            TriggerControl(NewTriggerValue, 1, ref _gamepad.updateRightTrigger);
        }

        private void TriggerControl(double NewTriggerValue, int scalor, ref bool updateTriggerBool)
        {
            if (updateTriggerBool)
            {
                if (MotorZ.IsOpen)
                {
                    updateTriggerBool = false;
                    InitiateVelocityMode(MotorZ, _ZaxisMotorAcceleration);
                    double speed = NewTriggerValue * scalor * ZaxisGamepadMaxSpeed;
                    if (_gamepad.A)
                    {
                        speed = speed / 5;
                    }
                    AddLineToConsole("LM: " + speed.ToString());
                    MotorZ.SetTargetVelocity((int)speed);
                    updateTriggerBool = true;
                }
                else
                {
                    AddLineToConsole("Z-Achse: " + MotorNotReady);
                }
            }
        }

        private void _gamepad_LeftTriggerValueChange(double NewTriggerValue)
        {
            TriggerControl(NewTriggerValue, -1, ref _gamepad.updateLeftTrigger);
        }

        private void _gamepad_RightThumbstickValueChange(Thumbstick NewThumbstickState)
        {
            if (_gamepad.updateRightThumbstick)
            {
                _gamepad.updateRightThumbstick = false;
                if (MotorY.IsOpen)
                {
                    InitiateVelocityMode(MotorY, _YaxisMotorAcceleration);
                    double speed = NewThumbstickState.X * YaxisGampadMaxSpeed;
                    if (_gamepad.A)
                    {
                        speed = speed / 5;
                    }
                    MotorY.SetTargetVelocity((int)speed);
                }
                else
                {
                    AddLineToConsole("Y-Achse: " + MotorNotReady);
                }
                _gamepad.updateRightThumbstick = true;

            }
        }

        async void _gamepad_LeftThumbstickValueChange(Thumbstick NewThumbstickState)
        {
            if (_gamepad.updateLeftThumbstick)
            {
                _gamepad.updateLeftThumbstick = false;

                if (MotorX1.IsOpen && MotorX2.IsOpen)
                {
                    InitiateVelocityMode(MotorX1, _XaxisMotorAcceleration);
                    InitiateVelocityMode(MotorX2, _XaxisMotorAcceleration);

                    double speedST = NewThumbstickState.Y * XaxisGampadMaxSpeed;
                    if (_gamepad.A)
                    {
                        speedST = speedST / 5;
                    }
                    AddLineToConsole("ST: " + speedST.ToString());
                    List<Task> moves = new List<Task>();
                    moves.Add(Task.Run(() => MotorX1.SetTargetVelocity((int)speedST)));
                    moves.Add(Task.Run(() => MotorX2.SetTargetVelocity((int)speedST)));
                    await Task.WhenAll(moves);
                }
                else
                {
                    AddLineToConsole("X-Achse: " + MotorNotReady);
                }
                _gamepad.updateLeftThumbstick = true;
            }
        }

        private void InitiateVelocityMode(Motor m, int MotorAcceleration)
        {
            if(m.CurrentMode != "VelocityMode")
            {
                m.VelocityMode(0);
                m.SetAcceleration(MotorAcceleration);
            }
        }

        private void _gamepad_ConnectionStatusChanged(bool isConnected)
        {
            CBcontrollerVerbunden.Dispatcher.Invoke(new Action(() => { CBcontrollerVerbunden.IsChecked = isConnected; }));
            CBactivateController.Dispatcher.Invoke(new Action(() => {
                CBactivateController.IsEnabled = isConnected;
                CBactivateController.IsChecked = false;
            }));
            UpdateGUIControllerBatteryStatus();
        }

        private void UpdateGUIControllerBatteryStatus()
        {
            try
            {
                string status = _gamepad.GetBatteryStatus();
                LblBatterieStatus.Dispatcher.Invoke(new Action(() => { LblBatterieStatus.Content = status; }));
            }
            catch (SlimDX.XInput.XInputException)
            {
                LblBatterieStatus.Dispatcher.Invoke(new Action(() => { LblBatterieStatus.Content = "n.a."; }));
            }

        }

        private void AddLineToConsole(string NewLine)
        {
            ConsoleText.Dispatcher.Invoke(new Action(() =>
           {
               Console.WriteLine("<" + DateTime.Now.ToString("hh:mm:ss") + "> " + NewLine);
               int i = ConsoleText.Text.Length;

               int nLines = ConsoleText.Text.Split(Environment.NewLine.ToCharArray()).Length;
               if (nLines > 100)
               {
                   string[] lines = ConsoleText.Text.Split('\n').Skip(1).ToArray();
                   ConsoleText.Text = string.Join("\n", lines);
               }

               ConsoleText.SelectionStart = i;
               ConsoleText.ScrollToEnd();
           }));
        }

        // MotorControlGUI:
        private void ShowMotorControls()
        {
            _motorControlGUI = new MotorControlGUI();
            _motorControlGUI.Show();
            SubscribeToMCGevents();
        }

        private void SubscribeToMCGevents()
        {
            _motorControlGUI.GUIstartMotion += _motorControlGUI_GUIstartMotion;
            _motorControlGUI.GUIstopMotion += _motorControlGUI_GUIstopMotion;
            _motorControlGUI.Closing += MotorControlGUI_Closing;
        }

        private void UnsubscribeToMCGevents()
        {
            _motorControlGUI.Closing -= MotorControlGUI_Closing;
            _motorControlGUI.GUIstartMotion -= _motorControlGUI_GUIstartMotion;
            _motorControlGUI.GUIstopMotion -= _motorControlGUI_GUIstopMotion;
        }

        private void _motorControlGUI_GUIstopMotion(int[] CorrespondingVector)
        {
            AddLineToConsole("stop " + CorrespondingVector[0].ToString() + "|" + CorrespondingVector[1] + "|" + CorrespondingVector[2]);
        }

        private void _motorControlGUI_GUIstartMotion(int[] CorrespondingVector)
        {
            AddLineToConsole("start " + CorrespondingVector[0].ToString() + "|" + CorrespondingVector[1] + "|" + CorrespondingVector[2]);
        }

        private void MotorControlGUI_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            UnsubscribeToMCGevents();
        }

        // COM Port selection functions:

        private void ChangeCOMPort(ComboBox cb, ListBoxItem lbi)
        {
            if (startupcounter > 3)
            {
                string COMport = lbi.Content.ToString();
                string Tag = cb.Tag.ToString();

                TryReconnect(COMport, cb);
            }
            else { startupcounter++; }
        }

        private void CloseConnection(string Tag)
        {
            TagToComboBox[Tag].Foreground = Brushes.Red;
            if (TagToMotor[Tag].IsOpen)
            {
                TagToMotor[Tag].ClosePort();
            }
        }

        private void HandleConnectionFail(ComboBox _cb, Exception e)
        {
            MessageBox.Show(e.Message, ConnectionErrorHeader, MessageBoxButton.OK, MessageBoxImage.Information);
            _cb.Foreground = Brushes.Red;
        }

        private void TryReconnect(string COMport, ComboBox _cb)
        {
            string Tag = _cb.Tag.ToString();

            try
            {
                if (TagToMotor[Tag].IsOpen)
                {
                    TagToMotor[Tag].ClosePort();
                }
                TagToMotor[Tag].ConnectViaCOM(COMport);
                if (TagToMotor[Tag].isConfirmed)
                {
                    _cb.Foreground = Brushes.Green;
                    TagToLabel[Tag].Content = ConnectedMessage;
                    SetupMotor(Tag);
                }
                else
                {
                    HandleConnectionFail(_cb, new Exception(WrongMotor));
                }
            }
            catch (System.IO.IOException exception)
            {
                HandleConnectionFail(_cb, exception);
            }
            catch (UnauthorizedAccessException exception)
            {
                HandleConnectionFail(_cb, exception);
            }
        }

        private void SetupMotor(string Tag)
        {
            TagToMotor[Tag].ErrorOccured += Motor_ErrorOccured;
            TagToMotor[Tag].CommunicationHappened += Motor_CommunicationHappened;
            TagToMotor[Tag].Tag = Tag;
        }

        private void Motor_CommunicationHappened(string Tag, string ComOut, string ComIn)
        {
            Debug.WriteLine(Tag + ": " + ComOut + " > " + ComIn);
        }

        private void Motor_ErrorOccured(string Tag, string ErrorMessage)
        // this method is called by an event when an error in any of the motors happens
        {
            AddLineToConsole("Error in " + Tag + " axis motor: " + ErrorMessage);
        }


        // Motor Controls:

        private void StopAllMotors()
        {
            foreach (Motor m in AllMotors)
            {
                if (m.IsOpen)
                {
                    m.StopMotion();
                }
            }
        }

        private void FreeMotorZFromEndswitch()
        {
            if (MotorZ.IsOpen)
            {
                int LastModeOfOP = MotorZ.InitiateFreeFromEndswitch();
                MessageBox.Show(FreeMotorMessage, InstructonHeader, MessageBoxButton.OK);
                MotorZ.FreeFromEndswitch(LastModeOfOP);
            }
        }

        private void ResetMotorByTag(string Tag)
        {
            if (Tag != "all")
            {
                if (TagToMotor[Tag].IsOpen)
                {
                    TagToMotor[Tag].FaultReset();
                    AddLineToConsole("Motor " + Tag + "wurde resettet");
                }
            }
            else
            {
                foreach (Motor m in AllMotors)
                {
                    if (m.IsOpen) { m.FaultReset(); }
                }
                AddLineToConsole("Alle verbundenen Motoren wurden resettet");
            }
        }

        private void ZeroMotorByTag(string Tag)
        {
            switch (Tag)
            {
                case "X":
                    SetMotorPosZero(MotorX1);
                    SetMotorPosZero(MotorX2);
                    break;
                case "Y":
                    SetMotorPosZero(MotorY);
                    break;
                case "Z":
                    SetMotorPosZero(MotorZ);
                    break;
                case "all":
                    foreach (Motor m in AllMotors)
                    {
                        SetMotorPosZero(m);
                    }
                    break;
            }
        }

        private void SetMotorPosZero(Motor m)
        {
            if (m.IsOpen)
            {
                m.SetCurrentPositionToZero();
                AddLineToConsole("Motor " + m.Tag + " Position wurde genullt");
            }
        }

        async void Move3AxisAbs(int[] coordinate)
        {
            int[] CurrentPos = new int[] { 0, 0, 0 };

            for (int i = 0; i < 3; i++)
            {
                if (IndexToMotor[i].IsOpen)
                {
                    CurrentPos[i] = IndexToMotor[i].GetPosition();
                }
            }
            int[] speedValues = MotorValueCoordinateConverter.CalculateSpeedValues(CurrentTypedPosition, CurrentPos, _MoveSpeed);

            List<Task> moves = new List<Task>();
            AddToMoveTaskList(ref moves, MotorX1, speedValues[0], coordinate[0]);
            AddToMoveTaskList(ref moves, MotorX2, speedValues[0], coordinate[0]);
            AddToMoveTaskList(ref moves, MotorY, speedValues[1], coordinate[1]);
            AddToMoveTaskList(ref moves, MotorZ, speedValues[2], coordinate[2]);

            await Task.WhenAll(moves);
        }

        private void AddToMoveTaskList(ref List<Task> moves, Motor m, int speedValue, int coordinate)
        {
            if (m.IsOpen)
            {
                moves.Add(Task.Run(() =>
                {
                    m.SetMoveVelocity(speedValue);
                    m.MoveAbs(coordinate);
                }));
            }
        }

        // General UI functions:
        private string GetObjectTag(object obj)
        {
            string Tag = (obj as Button).Tag.ToString();
            return Tag;
        }

        //  -----------------------------------------UI Control functions---------------------------------------------
        private void BtnSteuerung_Click(object sender, RoutedEventArgs e)
        {
            ShowMotorControls();
        }

        // Console functionalities:
        private void ConsoleText_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {

                string addition = ConsoleText.Text.Split('\n').Last();
                ConsoleText.Text = ConsoleText.Text.Remove(ConsoleText.Text.Length - addition.Length, addition.Length);
                AddLineToConsole(addition);
                switch (addition)
                {
                    case "getPos":
                        AddLineToConsole(MotorX1.GetPosition().ToString());
                        break;
                    case "Pos":
                        PositionUpdateTimer.Start();
                        break;
                    case "PosS":
                        PositionUpdateTimer.Stop();
                        break;
                    case "error":
                        string err = MotorZ.ReadCurrentError();
                        AddLineToConsole(err);
                        break;
                    case "errorx1":
                        string tst = MotorX2.sendMessage("1001", "00");
                        string tst2 = MotorX2.sendMessage("603F", "00");
                        string wa = MotorX2.sendMessage("6040", "00", 0x80);
                        break;
                    case "spd":
                        ZmeasureSpeed = 10000;
                        break;
                    default:
                        if (addition.Contains("/"))
                        {
                            addition = addition.Remove(0, 1);
                            AddLineToConsole(MotorZ.sendRawMessage(addition, true));
                        }
                        break;

                }
            }
        }

        private void ConsoleText_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            int currentCommandLength = ConsoleText.Text.Split('\n').Last().Length;
            if (e.Key == Key.Back && ConsoleText.SelectionStart <= ConsoleText.Text.Length-currentCommandLength)
            {
                e.Handled = true;
            }
            else if(ConsoleText.SelectionStart <= ConsoleText.Text.Length - currentCommandLength - 1 && e.Key != Key.Left && e.Key != Key.Right && e.Key != Key.Up && e.Key != Key.Down)
            {
                e.Handled = true;
            }
        }

        private void ConnectionChoiceSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ChangeCOMPort(sender as ComboBox, e.AddedItems[0] as ListBoxItem);
        }

        private void ReconnectBtnClick(object sender, RoutedEventArgs e)
        {
            string Tag = GetObjectTag(sender);
            string COMport = TagToComboBox[Tag].SelectionBoxItem.ToString();
            TryReconnect(COMport, TagToComboBox[Tag]);
        }

        private void CBactivateController_Click(object sender, RoutedEventArgs e)
        {
            if((sender as CheckBox).IsChecked.GetValueOrDefault())
            {
                ControllerUpdateTimer.Start();
            }
            else
            {
                ControllerUpdateTimer.Stop();
            }
        }

        private void BtnStop_Click(object sender, RoutedEventArgs e)
        {
            StopAllMotors();
        }

        private void BtnFreifahren_Click(object sender, RoutedEventArgs e)
        {
            FreeMotorZFromEndswitch();
        }

        private void BtnReferenz_Click(object sender, RoutedEventArgs e)
        {
            if (MotorZ.IsOpen) { MotorZ.Reference(); }
        }

        private void BtnClose(object sender, RoutedEventArgs e)
        {
            string Tag = GetObjectTag(sender);
            CloseConnection(Tag);
        }

        private void BtnReset(object sender, RoutedEventArgs e)
        {
            string Tag = GetObjectTag(sender);
            ResetMotorByTag(Tag);
        }

        private void BtnZero(object sender, RoutedEventArgs e)
        {
            string Tag = GetObjectTag(sender);
            ZeroMotorByTag(Tag);
        }

        private void BtnPosAnfahren_Click(object sender, RoutedEventArgs e)
        {
            Move3AxisAbs(CurrentTypedPosition);
            AddLineToConsole("pos: " + CurrentTypedPosition[0].ToString() + " | " + CurrentTypedPosition[1].ToString() + " | " + CurrentTypedPosition[2].ToString());
        }

        private void BtnOpenCSV_Click(object sender, RoutedEventArgs e)
        {
            TaskStepDataTable.Columns.Add("X");
            TaskStepDataTable.Columns.Add("Y");
            TaskStepDataTable.Columns.Add("Z");
            TaskStepDataTable.Columns.Add("Task");

            OpenFileDialog CSVFileDlg = new OpenFileDialog();
            Nullable<bool> result = CSVFileDlg.ShowDialog();
            string path = "";
            if(result == true)
            {
                path = CSVFileDlg.FileName;
            }
            List<TaskStep3Axis> tslist = CSVinput.ReadFile(path);

            CSVinput.LoadCSVdataintoDataTable(ref TaskStepDataTable, tslist);
            TaskStepDataGrid.ItemsSource = TaskStepDataTable.DefaultView;
        }
    }
}
